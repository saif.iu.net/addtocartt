import 'package:flutter/material.dart';
import 'package:flutter_application_test1/model/productModel.dart';
import 'package:flutter_application_test1/viewmodel/addToCartVM.dart';
import 'package:flutter_application_test1/views/cart.dart';
import 'package:flutter_application_test1/views/widgets/cartCounter.dart';
import 'package:flutter_application_test1/views/widgets/productItem.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';

class ProductScreen extends StatefulWidget {
  @override
  _ProductScreenState createState() => _ProductScreenState();
}

class _ProductScreenState extends State<ProductScreen> {
  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        toolbarHeight: 50,
        actions: [
          InkWell(
            onTap: () {
              Get.to(CartScreen());
            },
            child: Padding(
              padding:
                  const EdgeInsets.only(left: 0, right: 15, top: 8, bottom: 8),
              child: Stack(
                children: [
                  Align(
                      alignment: Alignment.bottomCenter,
                      child: Icon(Icons.shopping_cart_rounded,
                          color: Colors.blue, size: 25)),
                  Positioned(
                    top: 0,
                    left: 0,
                    right: 0,
                    child: GetBuilder<AddToCartVM>(
                      // specify type as Controller
                      init: AddToCartVM(), // intialize with the Controller
                      builder: (value) => CartCounter(
                        count: value.lst.length.toString() ?? "0",
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
        leading: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Icon(Icons.menu_rounded, color: Colors.blue, size: 25),
        ),
        title: Center(
          child: Text(
            "My Mart",
            style: TextStyle(color: Colors.blue),
          ),
        ),
      ),
      body: SafeArea(
          child: SingleChildScrollView(
        child: Container(
          height: screenSize.height * 0.24,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: prds.length,
            itemBuilder: (context, index) => ProductItem(
              screenSize: screenSize,
              image: prds[index]["image"],
              itemName: prds[index]["name"],
            ),
          ),
        ),
      )),
    );
  }
}
