class Products {
  Products({
    this.name,
    this.image,
  });

  String name;
  String image;

  Map<String, dynamic> toJson() => {
        "name": name,
        "image": image,
      };
}

final prds = [
  {
    "name": "ABCD",
    "image":
        "https://lapntab.com/storage/app/public/images/products/oppo-a12_1.jpg"
  },
  {
    "name": "QQWE",
    "image":
        "https://lapntab.com/storage/app/public/images/products/oppo-f15_1.jpg"
  },
  {
    "name": "WWSA",
    "image":
        "https://lapntab.com/storage/app/public/images/products/reno-5_1.jpg"
  },
  {
    "name": "EXMP",
    "image":
        "https://lapntab.com/storage/app/public/images/products/apple-iphone-11-pro-64gb_1.jpg"
  },
  {
    "name": "SADS",
    "image":
        "https://lapntab.com/storage/app/public/images/products/hp-envy-13-ah1011tx-8th-gen_1.jpg"
  },
  {
    "name": "SADS",
    "image":
        "https://lapntab.com/storage/app/public/images/products/hp-pavilion-15-cs1034tx-i7_1.jpg"
  },
];
